package yousef.test;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity mainActivity = null;
    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(DetailsActivity.class.getName(),null,false);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
    }

    @Test
    public void test(){

        assertNotNull(mainActivity.rv_articles);
        onView(withId(R.id.cv_item)).perform(click());
        Activity activity = getInstrumentation().waitForMonitorWithTimeout(monitor,5000);
        assertNotNull(activity);

    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}