package yousef.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv_articles;
    public static ArticleModel myArticlesModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv_articles = (RecyclerView) findViewById(R.id.rv_articles);
        call();


    }

    private void call() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, constants.API_MAIN_URL + constants.API_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new GsonBuilder().create();
                myArticlesModel = gson.fromJson(response, ArticleModel.class);

                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                rv_articles.setLayoutManager(layoutManager);
                ArticlesAdapter adapter = new ArticlesAdapter(MainActivity.this, myArticlesModel);
                rv_articles.setAdapter(adapter);
            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "failed loading data", Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        stringRequest.setTag("TAG");
    }

}
