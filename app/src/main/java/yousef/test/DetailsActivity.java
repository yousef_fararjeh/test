package yousef.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    ImageView image;
    TextView author, title, description, date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        image = (ImageView) findViewById(R.id.details_image);
        author = (TextView) findViewById(R.id.details_author);
        title = (TextView) findViewById(R.id.details_title);
        description = (TextView) findViewById(R.id.details_description);
        date = (TextView) findViewById(R.id.details_date);

        try {
            Picasso.with(this).load(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getMedia().get(0).getMediaMetadata().get(2).getUrl()).into(image);
        } catch (Exception e) {
            Picasso.with(this).load(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getMedia().get(0).getMediaMetadata().get(1).getUrl()).into(image);
        }

        author.setText(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getByline());
        title.setText(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getTitle());
        description.setText(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getAbstract());
        date.setText(MainActivity.myArticlesModel.getResults().get(getIntent().getIntExtra("position", 0)).getPublishedDate());
    }

}
