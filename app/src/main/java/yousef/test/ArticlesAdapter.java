package yousef.test;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.myDataList> {
    LayoutInflater inflater;
    ArticleModel modelDatas;
    Context context;


    public ArticlesAdapter(Context context, ArticleModel modelDatas) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.modelDatas = modelDatas;
    }

    @Override
    public ArticlesAdapter.myDataList onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.article_item, parent, false);
        ArticlesAdapter.myDataList modelDatas = new ArticlesAdapter.myDataList(v);
        return modelDatas;
    }

    @Override
    public void onBindViewHolder(ArticlesAdapter.myDataList holder, final int position) {

        holder.tvArticleTitle.setText(modelDatas.getResults().get(position).getTitle());
        holder.tvArticleAuthor.setText(modelDatas.getResults().get(position).getByline());
        Picasso.with(context).load(modelDatas.getResults().get(position).getMedia().get(0).getMediaMetadata().get(0).getUrl()).into(holder.articleImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (modelDatas != null)
            return modelDatas.getResults().size();
        else
            return 0;
    }


    class myDataList extends RecyclerView.ViewHolder {
        TextView tvArticleTitle;
        TextView tvArticleAuthor;
        CircleImageView articleImage;

        public myDataList(View itemView) {
            super(itemView);
            articleImage = (CircleImageView) itemView.findViewById(R.id.article_image);
            tvArticleTitle = (TextView) itemView.findViewById(R.id.article_title);
            tvArticleAuthor = (TextView) itemView.findViewById(R.id.article_author);


        }

    }
}
